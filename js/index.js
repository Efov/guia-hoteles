$(function(){
    $("[data-toggle='tooltip']").tooltip();
     $('[data-toggle="popover"]').popover();
    //$("input[type='number']").inputSpinner();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal',function(e){
        console.log('El modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactobtn').addClass('btn-primary');
        $('#contactobtn').prop('disable',true);
    });
    $('#contacto').on('shown.bs.modal',function(e){
        console.log('El modal Contacto se  mostro');
    });

    $('#contacto').on('hide.bs.modal',function(e){
        console.log('El modal Contacto se oculta');
    });
    $('#contacto').on('hidden.bs.modal',function(e){
        console.log('El modal Contacto se  Oculto');
        $('#contactobtn').prop('disable',false);
    });
});